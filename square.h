
#ifndef SQUARE_H
#define SQUARE_H

#include "figure.h"

class Square: public IFigure{
public:
    Square(double);
    double shape() const override;
    double perimetr() const override;
    double size() const;
private:
    double _sideLength;
};

#endif