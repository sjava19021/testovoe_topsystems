#include "rectangle.h"

Rectangle::Rectangle(double l, double w)
    :_length(l), _width(w)
{

}

double Rectangle::perimetr() const{
    return 2 * _length + 2*_width;
}

double Rectangle::shape() const{
    return _length*_width;
}

double Rectangle::length() const{
    return _length;
}

double Rectangle::width() const{
    return _width;
}