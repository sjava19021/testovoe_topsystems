#include "drawer.h"
#include <iostream>

Drawer::Drawer(){
    _window =  new sf::RenderWindow(sf::VideoMode(800, 600), "My window");
}

void Drawer::draw(Square* s){
    if(s)
        _squares.push_back(s);
}

void Drawer::draw(Rectangle* r){
    if(r)
        _rectangles.push_back(r);
}

void Drawer::draw(Circle *c){
    if(c)
        _circles.push_back(c);
}

void Drawer::display(){
    while (_window->isOpen())
    {
        sf::Event event;
        while(_window->pollEvent(event)){
            if(event.type == sf::Event::Closed)
            _window->close();
        }
        _window->clear(sf::Color::Black);

        for(int i = 0; i < _circles.size(); ++i){
            sf::CircleShape shape(_circles[i]->radius());
            shape.setFillColor(sf::Color(100, 250, 50));
            _window->draw(shape);
        }

        for(int i = 0; i < _rectangles.size(); ++i){
            sf::RectangleShape shape(sf::Vector2f(_rectangles[i]->length(), _rectangles[i]->width()));
            shape.setFillColor(sf::Color(100, 250, 50));
            _window->draw(shape);
        }

        for(int i = 0; i < _squares.size(); ++i){
            sf::RectangleShape shape(sf::Vector2f(_squares[i]->size(), _squares[i]->size()));
            shape.setFillColor(sf::Color(100, 250, 50));
            _window->draw(shape);
        }
            
        _window->display();
    } 
}


