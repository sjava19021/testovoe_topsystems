#include <SFML/Graphics.hpp>
#include <iostream>

#include "square.h"
#include "rectangle.h"
#include "circle.h"
#include "drawer.h"


int main(){
    Circle c(100);
    Rectangle r(30, 40);
    Drawer d;
    d.draw(&c);
    d.draw(&r);
    d.display();
    
    
    return 0;
}
