#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "figure.h"
#include <utility>


class Rectangle: public IFigure{
public:
    Rectangle(double, double);
    double perimetr() const override;
    double shape() const override;
    double length() const;
    double width() const;
private:
    double _length;
    double _width;
};

#endif