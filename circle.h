#ifndef CIRCLE_H
#define CIRCLE_H

#include "figure.h"

class Circle : public IFigure{
public:
    Circle(double);
    double perimetr() const override;
    double shape() const override;
    double radius() const;
private:
    double _radius;
};

#endif